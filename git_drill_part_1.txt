mkdir git_sample_project
cd git_sample_project
git init 
touch a.txt b.txt c.txt
vi a.txt
this is a text file named 'a'
:wq
vi b.txt
this is a text file named 'b'
:wq
vi c.txt
this is a text file named 'c'
:wq
git add a.txt b.txt
git status
git commit -m "Add a.txt and b.txt"
git status
git add c.txt
git commit -m 'Add c.txt'
git remote add origin https://gitlab.com/MrSinghal/git_drill.git
git push origin master
cd ..
mkdir git_sample_project_2
cd git_sample_project_2
git clone https://gitlab.com/MrSinghal/git_drill.git
touch d.txt;
git init
git add d.txt
git commit -m 'Add d.txt'
git remote add origin https://gitlab.com/MrSinghal/git_drill.git
git pull origin master --allow-unrelated-histories
git push -u origin master



